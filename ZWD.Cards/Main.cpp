#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};
enum Suit
{
	Hearts,
	Spades,
	Diamons,
	Clubs
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	(void)_getch();
	return 0;
}